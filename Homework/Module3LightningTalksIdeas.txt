Senior Thesis Ideas: Module 3

Idea 1:
Checking Python Code for Beginners

This program would aid coders who are learning Python code for the first time.
It will scan the code and check for errors such as improper indentation or
poor syntax and it would then suggest solutions for the errors. This could also
be used for Java if possible. It is tricky for beginners to grasp what the
errors represent when they are still trying to learn Python code.

Idea 2:
Spell Check for Latex
Latex does not have spell check installed into it. This can cause many errors
in the paper if you are not careful. I think that Latex needs to have a spell
check.

Idea 3:
Checking search engines results
Enter multiple different searches into a search
engine and check to see if the results are accurate for what the search is
looking for. Would have to have previous information about these topics. Check
the links and then scan for matching information.

Idea 4:
Hacking Passwords
Create a program that will try to hack into a person's account. It will test to
see if the system is strong enough to handle multiple attempts or if it will
eventually give way. This will help test to see if there is enough security
or if the system is flawed and needs to be fixed.
